REM AHN3 Uniform
python ahn_opgaande.py c:\apps\temp_geodata\AHN3\AHN3-DSM-5m.tif c:\apps\temp_geodata\AHN3\AHN3-DTM-5m.tif ahn3_opgaande_elementen_uniform c:\apps\temp_geodata\AHN3\sample\temp --min_diff 3 --mv_value 1 --u_height 10 --filter_powerlines
python .\postprocessing\pad_crop.py c:\apps\temp_geodata\AHN3\sample\temp\ahn3_opgaande_elementen_uniform.tif c:\apps\temp_geodata\AHN3\sample\ahn3_opgaande_elementen_uniform.tif 10000 300000 280000 625000
python .\postprocessing\postprocessing.py c:\apps\temp_geodata\AHN3\sample\ahn3_opgaande_elementen_uniform.tif c:\apps\temp_geodata\AHN3\sample\25m\ahn3_opgaande_elementen_uniform_25m.tif 25 5 0 max --flt

REM AHN3 variabel
python ahn_opgaande.py c:\apps\temp_geodata\AHN3\AHN3-DSM-5m.tif c:\apps\temp_geodata\AHN3\AHN3-DTM-5m.tif ahn3_opgaande_elementen c:\apps\temp_geodata\AHN3\sample\temp --min_diff 3 --mv_value 1 --filter_powerlines
python .\postprocessing\pad_crop.py c:\apps\temp_geodata\AHN3\sample\temp\ahn3_opgaande_elementen.tif c:\apps\temp_geodata\AHN3\sample\ahn3_opgaande_elementen.tif 10000 300000 280000 625000
python .\postprocessing\postprocessing.py c:\apps\temp_geodata\AHN3\sample\ahn3_opgaande_elementen.tif c:\apps\temp_geodata\AHN3\sample\25m\ahn3_opgaande_elementen_25m.tif 25 5 1 average --flt


REM AHN4 Uniform
REM python ahn_opgaande.py w:\projects\GeoDeskData\AHN4\DSM_5m w:\projects\GeoDeskData\AHN4\DTM_5m ahn4_opgaande_elementen_uniform c:\apps\temp_geodata\AHN4\sample --min_diff 3 --mv_value 0 --u_height 10 --filter_powerlines
python .\postprocessing\pad_crop.py c:\apps\temp_geodata\AHN4\sample\temp\ahn4_opgaande_elementen_uniform.tif c:\apps\temp_geodata\AHN4\sample\ahn4_opgaande_elementen_uniform.tif 10000 300000 280000 625000
python .\postprocessing\postprocessing.py c:\apps\temp_geodata\AHN4\sample\ahn4_opgaande_elementen_uniform.tif c:\apps\temp_geodata\AHN4\sample\25m\ahn4_opgaande_elementen_uniform.tif 25 5 0 average --flt

REM AHN4 variabel
REM python ahn_opgaande.py w:\projects\GeoDeskData\AHN4\DSM_5m w:\projects\GeoDeskData\AHN4\DTM_5m ahn4_opgaande_elementen c:\apps\temp_geodata\AHN4\sample --min_diff 3 --mv_value 0 --filter_powerlines
python .\postprocessing\pad_crop.py c:\apps\temp_geodata\AHN4\sample\temp\ahn4_opgaande_elementen.tif c:\apps\temp_geodata\AHN4\sample\ahn4_opgaande_elementen.tif 10000 300000 280000 625000
python .\postprocessing\postprocessing.py c:\apps\temp_geodata\AHN4\sample\ahn4_opgaande_elementen.tif c:\apps\temp_geodata\AHN4\sample\25m\ahn4_opgaande_elementen.tif 25 5 0 average --flt



