# Opgaande Elementen uit AHN


## About
Deze repository is ontwikkeld om __opgaande elementen__ te karteren vanuit [AHN DSM en DTM data](https://www.ahn.nl/producten). Opgaande elementen zijn een van de inputkaarten voor het [Viewscape](https://www.wur.nl/nl/show/ViewScape.htm) model.

## Requirements
Een python environment is benodigd met de volgende requirements:
 *  numpy
 *  pandas
 *  rasterio 
 *  skimage
 *  geopandas

## In short
De tool berekent het numerieke [verschil tussen het AHN DSM en DTM](https://gis.stackexchange.com/questions/5701/differences-between-dem-dsm-and-dtm). Waar het verschil een bepaalde drempelwaarde overschreidt, wordt een _opgaand element_ vastgesteld. Het opgaand element wordt gekarateriseerd door zijn hoogte, i.e. het DSM-DTM verschil.

De werking van de tool wordt hieronder ge-illustreerd: _luchtfoto_ --> _Digital Terrain Model_ --> _Digital Surface Model_ --> _Opgaande Elementen_.

![Luchtfoto](resources/01LuFo_small.png)
![Digital Terrain Model](resources/02DTM_small.png)
![Digital Sufrace Model](resources/03DSM_small.png)
![Opgaande elementen](resources/04bOpgaande_small.png)

## Usage
De tool wordt aangeroepen via `ahn_opgaande.py`. Er zijn vier verplichte parameters, plus acht optionele.

```
usage: ahn_opgaande.py [-h] [--sample SAMPLE] [--min_diff MIN_DIFF] [--mv_value MV_VALUE] [--w_width W_WIDTH]
                       [--w_height W_HEIGHT] [--of {flt,tif}] [--u_height U_HEIGHT] [--filter_powerlines]
                       dsm_src dtm_src out_name out_dir

positional arguments:
  dsm_src              Path to AHN DSM (file or directory).
  dtm_src              Path to AHN DTM (file or directory).
  out_name             Output filename.
  out_dir              Output directory.

options:
  -h, --help           Show this help message and exit
  --sample             Nr of sample blocks. Default 0.
  --min_diff           Minimum DSM-DTM difference. Default 2.
  --mv_value           Value for maaiveld. Default 0.
  --w_width            Window width in pxls. Default 1000.
  --w_height           Window height in pxls. Default 500.
  --of {flt,tif}       Output format. Default GeoTiff.
  --u_height           Uniform height for all opgaande elementen.
  --filter_powerlines  Majority filter below powerlines.
```

### Details
 *  De `dsm_src` en `dtm_src` parameters mogen verwijzen naar een losstaand DSM/DTM bestand, _of_ naar een directory met meerdere DSM/DTM bestanden. Denk hierbij aan een download van individuele [DSM](https://service.pdok.nl/rws/ahn/atom/dsm_05m.xml)/[DTM](https://service.pdok.nl/rws/ahn/atom/dtm_05m.xml) tegels. De tool veronderstelt dat evenveel DSM/DTM tegels worden aangeboden, dat gelijknamige DSM-DTM bestanden ruimtelijk met elkaar corresponderen en er geen ruimtelijke overlap is tussen de DSM/DTM files onderling.
 * Als een losstaand DSM/DTM bestand wordt aangeboden aan de tool, wordt deze meerdere `windows` geanalyseerd. Dit is om de geheugencapaciteit van een gemiddelde laptop niet te overschreiden. Een DSM/DTM bestand van bijvoorbeeld heel Nederland kan op deze manier worden geanalyseerd. 
 * Als de tool wordt verwezen naar een directory met DSM/DTM bestanden, wordt elk DSM-DTM bestand in z'n geheel geanalyseerd. Verondersteld wordt dat deze _tiles_ zo klein zijn dat ze de geheugencapaciteit van een gemiddelde laptop niet overschreiden.
* Een opgaand element is toegestaan op plekken waar de DSM _en_ DTM geldige waardes hebben (i.e. geen `NoData`). Omdat de DTM vaak `NoData` bevat op plekken van opgaande elementen (i.e. onder een boom), wordt `NoData` in het DTM ingevuld met omliggende waardes. Dit gebeurt met behulp van [gdal_fillnodata](https://gdal.org/programs/gdal_fillnodata.html).


### Opties
* Gebruik de `sample` parameter om de tool te testen. Geef als waarde op het aantal `windows` dat geanalyseerd moet worden (cw het aantal DSM/DTM files  uit de directory).
* Het minimale DSM-DTM verschil om te kwalificeren als opgaand element is instelbaar met de `min_diff` parameter. Default is 2 m.
* De numerieke waarde van pixels _zonder_ opgaande elementen (i.e. het maaiveld) wordt standaard op `0` gezet, maar kan aangepast worden naar een andere waarde met behulp van de `mv_value` parameter.
* De `w_width` en `w_height` parameters bepalen de breedte/hoogte (in pixels) van elk `window`.
* `of` (output format) bepaalt het bestandstype van de output. Gebruik `tif` voor [GeoTiff](https://gdal.org/drivers/raster/gtiff.html) of `flt` voor [ESRI BIL](https://gdal.org/drivers/raster/ehdr.html). Voor gebruik in ViewScape zijn `flt` files vereist.
* Met de `u_height` parameter kunnen alle opgaande elementen een uniforme hoogte worden toegekend. Dit is vergelijkbaar met de opgaande elementenkaarten zoals die gebruikelijk voor ViewScape werden gemaakt.

![Opgaande elementen uniform](resources/04aOpgaandeUniform_small.png)

* Hoogspanningslijnen zijn goed zichtbaar in het DSM, en verschijnen daardoor als opgaand element. Dit is niet altijd wenselijk. Door activatie van deze parameter wordt een [majority filter](https://scikit-image.org/docs/stable/api/skimage.filters.rank.html#skimage.filters.rank.majority) uitgevoerd rondom hoogspanningslijnen, waardoor deze (grotendeels) verdwijnen als opgaand element.

![Powerlines in DSM](resources/powerlines_dsm_small.png)
![Powerlines als opgaand element](resources/powerlines_opgaande_small.png)
![Powerlines als filtered](resources/powerlines_opgaande_filtered_small.png)


### Output
* De output heeft dezelfde resolutie als de DSM/DTM input bestand(en). 
* Als een losstaand DSM/DTM file wordt aangeboden, is de output raster-extent is gelijk aan de extent van de inputfiles. Als een directory met DSM/DTM files wordt aangeboden, wordt de output-extent gezet op _heel Nederland_. 
* Als de `--filter_powerlines` parameter is ingeschakeld, wordt het gefilterde bestand aangeleverd als `<out_dir>\<out_name>_filtered.(tif|flt)`.
* De tool produceert een geospatial raster volgens de specificaties van de gebruiker: `<out_dir>\<out_name>.(tif|flt)`. 
* De `*.flt` output bestaat uit het `*.flt` bestand zelf, plus een `*.hdr` _en_ een `*.hdr` specifiek bedoeld voor ViewScape. 
* Naast het geospatial raster produceert de tool een `*.DBF` tabel waarin de pixelwaardes worden verklaard. Deze tabel is een vereiste voor ViewScape.

![height_tabel](resources/hoogte_tabel.png)

* Alle parametersettings worden vastgelegd als metadata in de output.

![metadata](resources/image_metadata.png)
 


## Usage
`python ahn_opgaande.py W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DSM-5m.tif W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DTM-5m.tif c:/apps/opgaande_elementen_AHN3 TEST_opgaande_elementen --sample 10`

`python ahn_opgaande.py W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DSM-5m.tif W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DTM-5m.tif c:/apps/opgaande_elementen_AHN3 TEST_opgaande_elementen_uniform10m --sample 10 --u_height 10`

`python ahn_opgaande.py W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DSM-5m.tif W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DTM-5m.tif c:/apps/opgaande_elementen_AHN3 TEST_opgaande_elementen_ViewScape --sample 10 --of flt`

`python ahn_opgaande.py W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DSM-5m.tif W:/PROJECTS/Geodata/DataThematic/Hoogte/AHN3/AHN3-5m0-PDOK/HeelNL-Tiff/AHN3-DTM-5m.tif c:/apps/opgaande_elementen_AHN3 TEST_opgaande_elementen_HogerDan8 --sample 10 --min_diff 8`

`python ahn_opgaande.py w:\PROJECTS\GeoDeskData\AHN4\DSM_5m w:\PROJECTS\GeoDeskData\AHN4\DTM_5m c:/apps/opgaande_elementen_AHN3 TEST_opgaande_elementen_from_dir --sample 10`

## Support
Get in touch with [Hans](https://www.wur.nl/en/Persons/Hans-Roelofsen.htm).

## Authors and acknowledgment
* Auteur: Hans Roelofsen.
* Opdrachtgever: consortium [Monitor Landschap](https://monitorlandschap.maps.arcgis.com/home/gallery.html)
* Projectleider: [Jaap van Os](https://www.wur.nl/en/persons/jaap-van-os-1.htm)

## License
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
