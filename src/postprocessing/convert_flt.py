import numpy as np
import os
import rasterio as rio


def replace_hdr(dir: str, hdrname: str, output_profile: dict):
    """
    replace a header file with header format compatible with ViewScape
    :param dir: destination directory
    :param f: header filename
    :param output_profile:
    :return:
    """

    x_lower_left, y_lower_left = output_profile["transform"] * (
        0,
        output_profile["height"],
    )

    hdr = (
        f"ncols         {output_profile['width']}\n"
        f"nrows         {output_profile['height']}\n"
        f"xllcorner     {x_lower_left}\n"
        f"yllcorner     {y_lower_left}\n"
        f"cellsize      {output_profile['transform'].a}\n"
        f"NODATA_value  {output_profile['nodata']}\n"
        "byteorder     LSBFIRST"
    )

    # Rename old header
    os.rename(
        os.path.join(dir, f"{hdrname}.hdr"),
        os.path.join(dir, f"{hdrname}_original.hdr"),
    )

    with open(os.path.join(dir, f"{hdrname}.hdr"), "w") as f:
        f.write(hdr)


def convert_to_flt(src: str):
    """
    Take an GeoTiff image from disk and save as *.flt, compatible for ViewScape, including header file

    """

    image = rio.open(src)
    profile = image.profile
    profile.update(driver="EHdr", dtype=np.float32)

    with rio.open(f"{os.path.splitext(src)[0]}.flt", "w", **profile) as dest:
        dest.write(image.read(1), 1)

    replace_hdr(
        dir=os.path.dirname(src),
        hdrname=os.path.splitext(os.path.basename(src))[0],
        output_profile=profile,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("src", type=str, help="GeoTiff for conversion.")
    args = parser.parse_args()

    convert_to_flt(args.src)
