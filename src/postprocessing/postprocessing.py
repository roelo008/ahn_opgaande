from pathlib import Path
import sys
import os
import affine
import numpy as np
import rasterio as rio
from rasterio.warp import reproject

from convert_flt import convert_to_flt

sys.path.append(str(Path(__file__).parents[1]))
from hoogtes_dbf import heights_table_to_file, build_heights_table


def aggregate_geospatial_array(
    arr: np.ndarray,
    geospatial_profile: dict,
    target_resolution: int | float,
    aggregation_function: str,
) -> np.ndarray:
    """

    Parameters
    ----------
    arr
    geospatial_profile
    target_resolution
    aggregation_function

    Returns
    -------

    """

    target_height = int(
        (arr.shape[0] * getattr(geospatial_profile["transform"], "a"))
        // target_resolution
    )
    target_width = int(
        (arr.shape[1] * getattr(geospatial_profile["transform"], "a"))
        // target_resolution
    )
    destination = np.zeros(
        (target_height, target_width), dtype=geospatial_profile["dtype"]
    )
    aggregated_array, dst_transform = reproject(
        source=arr,
        destination=destination,
        src_transform=geospatial_profile["transform"],
        src_crs=geospatial_profile["crs"],
        dst_transform=affine.Affine(
            a=target_resolution,
            b=0,
            c=geospatial_profile["transform"].c,
            d=0,
            e=target_resolution * -1,
            f=geospatial_profile["transform"].f,
        ),
        dst_crs=geospatial_profile["crs"],
        src_nodata=geospatial_profile["nodata"],
        dst_nodata=geospatial_profile["nodata"],
        dest_resolution=(target_resolution, target_resolution),
        resampling=getattr(rio.enums.Resampling, aggregation_function),
    )
    return aggregated_array


def postprocessing_opgaande_elementen(
    src,
    destination,
    target_resolution,
    threshold: int,
    maaiveld_value: int,
    aggregation_function: str,
    as_flt,
):
    """
    Aggregate opgaande elementen geotiff to lower resolution, e.g. from 5 to 25m
    Low-res pixel is labelled 'opgaande elementen' if at least <threshold> high-res pixels are an opgaande element
    Aggregate the high-res pixel values by either average of max.

    Examples
    ----------

    input pixel, threshold = 5, aggregation function = average
    0 | 0 | 2 | 9 | 0
    0 | 0 | 6 | 2 | 0
    0 | 0 | 1 | 3 | 0
    0 | 0 | 0 | 0 | 0
    0 | 0 | 0 | 0 | 0
    output pixel
    |3.8|

    input pixel, threshold = 4, aggregation function = max
    0 | 0 | 1 | 0 | 0
    0 | 0 | 1 | 0 | 0
    0 | 0 | 0 | 0 | 0
    0 | 1 | 0 | 0 | 0
    0 | 0 | 0 | 0 | 0
    output pixel
    |0|

    Parameters
    ----------
    src
    destination
    target_resolution
    threshold
    maaiveld_value
    aggregation_function
    as_flt

    Returns
    -------

    """

    source = rio.open(src)
    source_profile = source.profile
    source_transform = source.transform

    for shape in ["width", "height"]:
        assert (
            np.divmod(getattr(source, shape), target_resolution)[1] == 0
        ), f"invalid target resolution for source {shape}"

    target_profile = source_profile.copy()
    target_profile.update(
        transform=affine.Affine(
            a=target_resolution,
            b=0,
            c=source_transform.c,
            d=0,
            e=target_resolution * -1,
            f=source_transform.f,
        ),
        width=int(
            np.divide(
                np.subtract(source.bounds.right, source.bounds.left), target_resolution
            )
        ),
        height=int(
            np.divide(
                np.subtract(source.bounds.top, source.bounds.bottom), target_resolution
            )
        ),
    )

    # Assert resolution and aggregation factor
    assert (
        target_resolution >= source.res[0]
    ), f"Invalid target resolution {target_resolution}, must be >= {source.res[0]}"
    assert threshold < np.square(
        np.divide(target_resolution, source.res[0])
    ), f"Invalid threshold {threshold}, must be less than {int(np.square(np.divide(target_resolution, source.res[0])))}."

    # Aggregate binary representation of opgaande elementen, to determine nr of high-res pixels in each output pixel
    opgaande_elementen_count = aggregate_geospatial_array(
        arr=np.where(source.read(1) > maaiveld_value, 1, 0),
        geospatial_profile=source_profile,
        target_resolution=target_resolution,
        aggregation_function="sum",
    )

    # Determine output pixels that meet treshold for minimum nr of high-res pixels
    valid_output_array = np.where(opgaande_elementen_count >= threshold, True, False)

    # Calculate aggregated value in the source array, meaningful functions are average (for actual height values),
    # or max for uniform height values
    aggregated_array = aggregate_geospatial_array(
        source.read(1),
        geospatial_profile=source_profile,
        target_resolution=target_resolution,
        aggregation_function=aggregation_function,
    )

    # Filter to valid output pixels
    output_array = np.where(valid_output_array, aggregated_array, maaiveld_value)

    with rio.open(destination, "w", **target_profile) as dest:
        dest.update_tags(
            sourcefile=source.name,
            target_resolution=target_resolution,
            dsm_src=source.tags()["dsm_src"],
            dtm_src=source.tags()["dtm_src"],
            pxl_threshold=threshold,
            created_by=os.environ.get("USERNAME"),
        )
        dest.write(output_array, 1)

    # Write height table to file
    heights_table_to_file(
        os.path.join(
            os.path.dirname(destination),
            f"{os.path.splitext(destination)[0]}_heights.dbf",
        ),
        build_heights_table(
            height_values=list(set(output_array.flatten()).difference({maaiveld_value})),
            maaiveld_value=maaiveld_value,
        ),
    )

    # Convert to flt if requested
    if as_flt:
        convert_to_flt(destination)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("src", type=str, help="GeoTiff for postprocessing.")
    parser.add_argument("destination", type=str, help="destination file.")
    parser.add_argument("res", type=int, help="target resolution.", default=25)
    parser.add_argument(
        "threshold",
        type=int,
        help="minimum nr of source pixels for valid output pixel.",
        default=5,
    )
    parser.add_argument("mv_value", type=int, help="maaiveld value.", default=0)
    parser.add_argument(
        "agg",
        type=str,
        help="aggregation function.",
        choices=[
            "min",
            "max",
            "average",
        ],
    )
    parser.add_argument(
        "--flt",
        action="store_true",
        help="also write flt.",
    )
    args = parser.parse_args()

    try:
        postprocessing_opgaande_elementen(
            args.src,
            args.destination,
            args.res,
            args.threshold,
            args.mv_value,
            args.agg,
            args.flt,
        )
    except (AssertionError, ValueError) as e:
        print(e)
        sys.exit(0)
