import affine
import numpy as np
import rasterio as rio
from rasterio.coords import BoundingBox
from rasterio.windows import Window
from typing import List,Tuple


def build_read_write_windows(
        source_raster: rio.DatasetReader, target_bounds: BoundingBox,
        target_transform: affine.Affine
) -> Tuple[List[Window], List[Window]]:
    """
    Determine intersection between target-raster-bounds and source-raster-bounds. Compile two lists of Windows:

    READ windows are referenced towards the source raster (using .transform).
      height = aggregation factor.
      width = width of intersected window, in pixels in combined_raster_resolution
      row, col offset is position of intersected window relative to combined raster

    WRITE windows are referenced towards the target raster.
      height = 1 pixel of target raster resolution
      width = width of intersected window, in pixels in target_resolution
      row, col offset is position of intersected window relative to target raster.

    If target raster extent is fully within combined raster extent, intersected window equals target window.

    Parameters
    ----------
    combined_raster

    Returns
    -------
    ([read_window01, read_window02 ...], [write_window01, write_window02 ...])

    """

    windows = []

    # Use Affine transformation of first the combined raster, then the target raster
    for rw, transform in zip(
            ["read", "write"], [source_raster.transform, target_transform]
    ):
        target_window = rio.windows.from_bounds(
            left=target_bounds.left,
            right=target_bounds.right,
            bottom=target_bounds.bottom,
            top=target_bounds.top,
            transform=transform,
        )
        read_window = rio.windows.from_bounds(
            left=source_raster.bounds.left,
            bottom=source_raster.bounds.bottom,
            right=source_raster.bounds.right,
            top=source_raster.bounds.top,
            transform=transform,
        )
        intersected_window = rio.windows.intersection(
            target_window, read_window
        )

        windows.append(
            [
                Window(
                    col_off=intersected_window.col_off,
                    row_off=i,
                    height=1,
                    width=intersected_window.width,
                )
                for i in range(
                int(intersected_window.row_off),
                int(intersected_window.row_off + intersected_window.height),
                1,
            )
            ]
        )

    assert len(windows[0]) == len(
        windows[1]
    ), f"Error {len(windows[0])} read windows and {len(windows[1])} write!"
    return windows[0], windows[1]


def crop_pad_raster(source_raster: str,
                    destination,
                    target_extent: Tuple[int],

                    ):
    """
    Crop or pad a raster on file to target extent. Do not change resolution

    Parameters
    ----------
    source_raster
    target_extent: (xmin, ymin, xmax, ymax)
    pad_value:

    Returns
    -------

    """

    source_raster = rio.open(source_raster)

    target_bounds = BoundingBox(left=target_extent[0], bottom=target_extent[1],
                                right=target_extent[2], top=target_extent[3])

    target_transform = affine.Affine(
        a=source_raster.res[0],
        b=0,
        c=target_bounds.left,
        d=0,
        e=source_raster.res[0] * -1,
        f=target_bounds.top
    )

    target_profile = source_raster.profile
    target_profile.update(
        height=np.divide(np.subtract(target_bounds.top, target_bounds.bottom),
                         source_raster.res[0]),
        width=np.divide(np.subtract(target_bounds.right, target_bounds.left),
                        source_raster.res[0]),
        transform=target_transform,
    )

    read_windows, write_windows = build_read_write_windows(
        source_raster,
        target_bounds,
        target_transform
    )

    with rio.open(destination, "w", **target_profile) as dest:
        dest.update_tags(**source_raster.tags())
        for i, rw in enumerate(zip(read_windows, write_windows), start=1):
            read_window, write_window = rw
            array = source_raster.read(1, window=read_window)
            dest.write(array, 1, window=write_window)


if __name__ == '__main__':

    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('src', type=str, help='source raster')
    parser.add_argument('dest', type=str, help='destination file')
    parser.add_argument(
        "xtnt",
        help="target extent: left bottom right top",
        type=int,
        nargs=4,
    )

    args = parser.parse_args()

    try:
        crop_pad_raster(
        source_raster=args.src,
        destination=args.dest,
        target_extent=args.xtnt
        )
    except ValueError as e:
        print(e)
        sys.exit(0)


