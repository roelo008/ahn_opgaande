import os
import random
from typing import Tuple

import affine
import numpy as np
import pandas as pd
import rasterio as rio
from rasterio.coords import BoundingBox
from rasterio.shutil import exists

DEFAULT_BOUNDS = BoundingBox(left=10000, bottom=300000, right=280000, top=625000)


def build_output_profile(**kwargs) -> Tuple[dict, str]:
    """
    Build output profile

    Parameters
    ----------
    kwargs

    Returns
    -------

    """

    if os.path.isfile(kwargs["dsm_src"]):
        dsm_sample_file = rio.open(kwargs["dsm_src"])
        origin = "file"
    elif os.path.isdir(kwargs["dsm_src"]):
        dsm_sample_file = rio.open(
            os.path.join(kwargs["dsm_src"], os.listdir(kwargs["dsm_src"])[0])
        )
        origin = "directory"
    else:
        print("unknown source")

    profile = rio.default_gtiff_profile.copy()
    profile.pop("nodata")
    profile.update(dtype=np.uint16, crs=dsm_sample_file.crs)
    profile.update(driver={"tif": "GTiff", "flt": "EHdr"}[kwargs["of"]])

    resolution = dsm_sample_file.res[0]

    profile.update(
        transform=affine.Affine(
            a=resolution,
            b=0,
            c=dsm_sample_file.bounds.left if origin == "file" else DEFAULT_BOUNDS.left,
            d=0,
            e=resolution * -1,
            f=dsm_sample_file.bounds.top if origin == "file" else DEFAULT_BOUNDS.top,
        )
    )

    profile.update(
        height=dsm_sample_file.shape[0]
        if origin == "file"
        else int((DEFAULT_BOUNDS.top - DEFAULT_BOUNDS.bottom) / resolution),
        width=dsm_sample_file.shape[1]
        if origin == "file"
        else int((DEFAULT_BOUNDS.right - DEFAULT_BOUNDS.left) / resolution),
        count=1,
        nodata=0,
    )

    return profile, origin


def get_ahn_files(**kwargs):
    """
    retrieve list of DSM and DTM files. Match based on filename
    Parameters
    ----------
    kwargs

    Returns
    -------

    """

    d = {}
    for x in ["dsm", "dtm"]:
        assert os.path.isdir(kwargs[f"{x}_src"])
        d[x] = [
            os.path.join(kwargs[f"{x}_src"], f)
            for f in os.listdir(kwargs[f"{x}_src"])
            if exists(os.path.join(kwargs[f"{x}_src"], f))
        ]
    dsms = pd.Series(d["dsm"]).sort_values()
    dtms = pd.Series(d["dtm"]).sort_values()
    assert dsms.shape == dtms.shape

    if kwargs["sample"] > 0:
        return random.sample(
            [(dsm, dtm) for dsm, dtm in zip(dsms, dtms)], kwargs["sample"]
        )
    else:
        return [(dsm, dtm) for dsm, dtm in zip(dsms, dtms)]
