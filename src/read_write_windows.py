import affine
import numpy as np
import random
from rasterio.windows import Window


class ReadWriteWindow:
    """
    A single read/write window and its associated unique combinations
    """

    def __init__(
        self,
        row_start: int,
        row_stop: int,
        column_start: int,
        column_stop: int,
        nr: int,
        top_left_x: int,
        top_left_y: int,
        resolution: int,
    ):
        self.rio_window = Window.from_slices(
            (row_start, row_stop), (column_start, column_stop)
        )

        self.transform = affine.Affine(
            a=resolution, b=0, c=top_left_x, d=0, e=resolution * -1, f=top_left_y
        )


def get_rw_windows(
    target_profile: dict,
    window_height: int = 2000,
    window_width: int = 2000,
    sample: int = 0,
) -> list:
    """ """

    target_width, target_height = (target_profile["width"], target_profile["height"])
    assert (
        np.divmod(target_height, window_height)[1] == 0
    ), f"window height {window_height} not conmensurable with raster height {target_height}"
    assert (
        np.divmod(target_width, window_width)[1] == 0
    ), f"window height {window_width} not conmensurable with raster width {target_width}"

    row_starts = range(0, target_profile["height"], window_height)
    column_starts = range(0, target_profile["width"], window_width)

    windows = []
    n = 0

    n_samples = sample

    for row_start in {True: random.sample(row_starts, n_samples), False: row_starts}[
        sample > 0
    ]:
        for column_start in {
            True: random.sample(column_starts, n_samples),
            False: column_starts,
        }[sample > 0]:
            windows.append(
                ReadWriteWindow(
                    row_start=row_start,
                    row_stop=row_start + window_height,
                    column_start=column_start,
                    column_stop=column_start + window_width,
                    nr=n,
                    top_left_x=(
                        target_profile["transform"] * (column_start, row_start)
                    )[0],
                    top_left_y=(
                        target_profile["transform"] * (column_start, row_start)
                    )[1],
                    resolution=getattr(
                        target_profile["transform"],
                        "a",
                    ),
                )
            )
            n += 1
    print(f"Created {n} windows of size {window_height}X{window_width}")

    return windows
