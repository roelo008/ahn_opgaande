import os
import affine
import rasterio as rio
import numpy as np
from typing import Tuple

import skimage.filters.rank
from skimage.morphology import disk
from rasterio.windows import Window
from rasterio.coords import BoundingBox
from rasterio.fill import fillnodata


def verify_matching_rasters(
    raster_a: rio.DatasetReader, raster_b: rio.DatasetReader
) -> Tuple[bool, AssertionError]:
    """

    Verify that two geospatial rasters have identical bounds and origin

    :param raster_a:
    :param raster_b:
    :return:
    """

    origin = (raster_a.transform.c, raster_a.transform.f) == (
        raster_b.transform.c,
        raster_b.transform.f,
    )
    resolution = raster_a.transform.a == raster_b.transform.a
    if origin and resolution:
        return True
    else:
        raise ValueError(f"{raster_a.name} doe not match {raster_b.name}")


def snap_bounds_outwards(bbox: BoundingBox, interval: int = 1000) -> BoundingBox:
    """
    Snap bounds from a GeoDataFrame to nearest 1000m
    Snap upwards for right and top
    Snap downwards for left and bottom
    :param interval:
    :return:
    """

    return BoundingBox(
        left=(bbox.left // interval) * interval,
        bottom=(bbox.bottom // interval) * interval,
        right=((bbox.right // interval) * interval) + interval,
        top=((bbox.top // interval) * interval) + interval,
    )


def bbox_to_window(
    bbox: BoundingBox, transform: affine.Affine
) -> Tuple[Window, affine.Affine]:
    """
    Transform bounds in projected coordinates to a rio Window
    :param bbox:
    :param transform:
    :return:
    """

    w = rio.windows.from_bounds(
        left=bbox.left,
        bottom=bbox.bottom,
        right=bbox.right,
        top=bbox.top,
        transform=transform,
    )
    t = affine.Affine(
        a=transform.a,
        b=transform.b,
        c=bbox.left,
        d=transform.d,
        e=transform.e,
        f=bbox.top,
    )

    return w, t


def fill_array(
    arr: np.array, nodata, max_search_distance: int = 50, si: int = 2
) -> np.array:
    """
    Interpolate NoData values in array

    """

    return fillnodata(
        image=arr,
        mask=np.where(arr == nodata, 0, 1),
        max_search_distance=max_search_distance,
        smoothing_iterations=si,
    )


def smooth_array(arr: np.array, disk_radius: int = 1) -> np.array:
    """
    Majority filter on binary array

    """

    return skimage.filters.rank.majority(arr, footprint=disk(disk_radius))


def verify_dsm_exceeds_dtm(
    dsm_array: np.ma.array, dsm_nodata, dtm_array: np.ma.array, dtm_nodata
) -> bool:
    """
    Verify that DSM values are always equal to or greater than DTM. Ignore NoData values

    Parameters
    ----------
    dsm_array
    dtm_array

    Returns
    -------

    """

    any_nodata = np.array((dsm_array == dsm_nodata) | (dtm_array == dtm_nodata))
    dsm_below_dtm = np.where(
        (dsm_array < dtm_array) & (any_nodata == False), True, False
    )

    return False if np.any(dsm_below_dtm) else True


def cap_dsm_to_dtm(dsm_array, dsm_nodata, dtm_array, dtm_nodata) -> np.array:
    """
    Set DSM equal to DTM where DSM < DTM

    Parameters
    ----------
    dsm_array
    dsm_nodata
    dtm_array
    dtm_nodata

    Returns
    -------

    """

    any_nodata = np.array((dsm_array == dsm_nodata) | (dtm_array == dtm_nodata))

    return np.where(
        (dsm_array < dtm_array) & (any_nodata == False), dtm_array, dsm_array
    )


def get_opgaande_elementen(
    dsm: rio.DatasetReader,
    dtm: rio.DatasetReader,
    minimum_difference: int = 3,
    maaiveld_value: int = 0,
    uniform_height: int = 0,
    **kwargs,
) -> np.array:
    """
    Read DSM and DTM for a certain bounding box. Return array with DSM-DTM differences
    where difference exceeds treshold cast to integer type

    Parameters
    ----------
    dsm_source
    dtm_source
    read_window
    minimum_difference
    maaiveld_value
    uniform_height

    Returns
    -------

    Details
    -------
    DSM   | DTM |   Difference | Opgaand Element (minimum_differnce = 3 | output
    10      08      02           False                                  | <maaiveld value>
    10      10      0            False                                  | <maaiveld value>
    10      06      04           True                                   | 04
    NoData  NoData  NoData       False                                  | NoData
    10      NoData  NoData       True                                   |
    -05     -08     03           True                                   | 03
    01      -02     03           True                                   | 03
    NoData  10      NoData       False                                  |

    """

    # dtm_array = dtm.read(1)#, window=read_window)
    dtm_array = fill_array(dtm.read(1, **kwargs), nodata=dtm.nodata)
    dsm_array = dsm.read(1, **kwargs)

    if np.all(dsm_array == dsm.nodata):
        # print('  All nodata, returning dummy array')
        return np.multiply(np.ones(shape=dsm_array.shape), maaiveld_value)

    verify = verify_dsm_exceeds_dtm(
        dsm_array=dsm_array,
        dsm_nodata=dsm.nodata,
        dtm_array=dtm_array,
        dtm_nodata=dtm.nodata,
    )
    if not verify:
        dsm_array = cap_dsm_to_dtm(dsm_array, dsm.nodata, dtm_array, dtm.nodata)

    # NoData mask
    any_nodata = np.array((dsm_array == dsm.nodata) | (dtm_array == dtm.nodata))

    # Generate array of opgaande elementen
    difference_array = np.subtract(
        np.where(any_nodata, 0, dsm_array), np.where(any_nodata, 0, dtm_array)
    )

    assert np.all(difference_array >= 0), f"Not all >= 0"

    # Threshold differences
    opgaande_elementen = np.where(
        difference_array >= minimum_difference,
        uniform_height if uniform_height > 0 else difference_array,
        maaiveld_value,
    )

    # If uniform height, also include pixles where DTM == NoData and DSM != NoData
    if uniform_height:
        opgaande_elementen = np.where(
            (dtm_array == dtm.nodata) & (dsm_array != dsm.nodata),
            uniform_height,
            opgaande_elementen,
        )

    # Cast difference array to integer
    return np.round(opgaande_elementen).astype(np.uint16)


# aoi = gp.read_file(r'c:\apps\temp_geodata\scratch.gpkg',
#                    layer='test_aoi')
# dsm_source = r'c:\apps\temp_geodata\AHN3\AHN3-DSM-5m.tif'
# dtm_source = r'c:\apps\temp_geodata\AHN3\AHN3-DTM-5m.tif'
# og, values = get_opgaande_elementen(dsm_source, dtm_source, aoi)
# bbox_snap = snap_bounds_outwards(aoi)
# prof = rio.default_gtiff_profile
# prof.update(width=og.shape[1], height=og.shape[0],
#             transform=affine.Affine(a=5, b=0, c=bbox_snap.left,
#                                     d=0, e=-5, f=bbox_snap.top),
#             count=1)
# with rio.open(r'c:\apps\temp_geodata\AHN3\sample\aoi01_test.tif', 'w', **prof) as dest:
#     dest.write(og, 1)
