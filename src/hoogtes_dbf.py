import os
import numpy as np
import geopandas as gp
import pandas as pd

COLUMN_NAMES = ["ID", "HEIGHT", "USE"]


def build_heights_table(height_values: list, maaiveld_value: int) -> gp.GeoDataFrame:
    """
    Build GeoDataFrame with column names and None Geometry, with ID and HEIGHT value for each value in height_values

    """

    return gp.GeoDataFrame(
        pd.concat(
            [
                pd.DataFrame(
                    data=[[maaiveld_value, 0, "maaiveld"]], columns=COLUMN_NAMES
                ),
                pd.DataFrame(
                    data={
                        COLUMN_NAMES[0]: height_values,
                        COLUMN_NAMES[1]: height_values,
                        COLUMN_NAMES[2]: "ahn opgaand element",
                    }
                ).sort_values(by=COLUMN_NAMES[0]),
            ],
            ignore_index=True,
        ).assign(geometry=None)
    ).astype({COLUMN_NAMES[0]: np.int32, COLUMN_NAMES[1]: np.int32})


def heights_table_to_file(destination: str, height_table: gp.GeoDataFrame) -> None:
    """
    Write heights table to file as *.dbf

    """

    height_table.to_file(f"{os.path.splitext(destination)[0]}.shp")

    # Remove redundant Shapefile components
    for f in os.listdir(os.path.dirname(destination)):
        if f.endswith(
            tuple(
                [
                    f"{os.path.splitext(os.path.basename(destination))[0]}{ext}"
                    for ext in [
                        ".shp",
                        ".shx",
                        ".prj",
                        ".sbn",
                        ".sbx",
                        ".cpg",
                        "shp.xml",
                    ]
                ]
            )
        ):
            os.remove(os.path.join(os.path.dirname(destination), f))
