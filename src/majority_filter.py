import os
import numpy as np
from rasterio.features import rasterize
import rasterio as rio
import geopandas as gp
from skimage.filters.rank import majority
from skimage.morphology import disk

VECTOR_SOURCES = {
    'powerlines': r"c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\RIVM\vw_nl_netlijnen_2023_v4_5_december\vw_nl_netlijnen_2023_v4_5_december.shp",
    'windturbines': r'c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\RIVM\Windturbines_2022_ashoogte\rivm_20230101_Windturbines_2022_ashoogte.shp'
}
TEMPORARY_FILENAME = r"majority_filtering_temporary"
MAJORITY_FILTER_DISK_SIZE = {
    'powerlines': 4,
    'windturbines': 6
}


def read_and_buffer_vector_source(what: str, buffer_size: int = 25, true_value: int = 1) -> gp.GeoDataFrame:
    """
    Read vector data and buffer around features

    Parameters
    ----------
    buffer_size: size in m around features
    true_value: integer in *is_feature* attribute to indicate presence of feature

    Returns
    -------

    """

    vector_data = gp.read_file(VECTOR_SOURCES[what])
    return vector_data.set_geometry(vector_data.buffer(buffer_size)).assign(is_feature=true_value)


def vector_to_raster(vector_data: gp.GeoDataFrame, target_field: str, raster_profile: dict):
    """
    Read, buffer and rasterize powerlines, write to raster using raster profile

    Parameters
    ----------
    profile

    Returns
    -------

    """

    print("Rasterizing vector data.")
    array = rasterize(
        shapes=[
            (x.geometry, getattr(x, target_field)) for i, x in vector_data.iterrows()
        ],
        merge_alg=rio.enums.MergeAlg.replace,
        transform=raster_profile["transform"],
        out_shape=(raster_profile["height"], raster_profile["width"]),
        fill=raster_profile["nodata"],
    ).astype(np.uint8)

    return array


def majority_filter(what: str, mask_array: np.array, mask_value: int, target_raster_src: str,
                    target_raster_minimum_value: int|float) -> np.array:
    """
    Apply majority filter on opgaande elementen array where mask_array == mask_value


    Parameters
    ----------
    mask_array: array indicating valid areas for majority filter
    mask_value: value in mask_array indicating where to apply majority filter
    target_raster_src: path to opgaande elementen array on disk
    target_raster_minimum_value: minimum value in target_raster for applying  majority filter. Useful for excluding 'maaiveld' pixels in opgaande elementenrasters

    Returns
    -------

    """

    print(f"Majority filter for {what}")

    opgaande_elementen = rio.open(target_raster_src).read(1)

    filter_array = np.where(
        (mask_array == mask_value) & (opgaande_elementen > target_raster_minimum_value), 1, 0
    ).astype(np.uint8)

    filtered_array = majority(filter_array, footprint=disk(MAJORITY_FILTER_DISK_SIZE[what]))

    # Return where original array was > maaiveld AND where filtering creates 0
    return np.where(
        (filtered_array == 0)
        & (opgaande_elementen > target_raster_minimum_value)
        & (mask_array == 1),
        target_raster_minimum_value, # Pas op, dit is de value die wordt teruggelegd op gefilterde plekken. Is alleen zinnig als dit de maaiveld_value is!
        opgaande_elementen,
    )


def filter_opgaande_elementen_based_on_vectors(vector_source: str, target_raster_src: str, **kwargs):
    """
    Read opgaande elementen array. Apply majority filter on areas indicated by a vector datasource.

    Parameters
    ----------
    vector_source: key to vector datasource containing areas where filtering should be applied.
                 supported are: "windturbines" and "powerlines"
    target_raster_src: raster on file on which filtering must be performed
    kwargs

    Returns
    -------

    Remarks
    -------
    An alternative approach would be to just remove all opgaande_elementen within the vector mask,
    ie to ditch the filtering all together.

    """

    profile = rio.open(target_raster_src).profile

    temporary_file = os.path.join(
        os.path.dirname(target_raster_src), f'{TEMPORARY_FILENAME}.{os.path.splitext(target_raster_src)[1]}'
    )
    destination_file = target_raster_src

    vectors = read_and_buffer_vector_source(what=vector_source)
    mask_raster = vector_to_raster(vector_data=vectors, target_field='is_feature', raster_profile=profile)
    filtered_array = majority_filter(what=vector_source, mask_array=mask_raster,
                            mask_value=1,
                            target_raster_src=target_raster_src,
                            target_raster_minimum_value=kwargs.get('mv_value', kwargs.get('minimum_value')))

    with rio.open(
            temporary_file,
            "w",
            **profile,
    ) as dest:
        dest.update_tags(**rio.open(target_raster_src).tags())
        dest.write(filtered_array, 1)

    # Remove original file
    os.remove(target_raster_src)

    # rename filtered file
    os.rename(temporary_file, destination_file)


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(usage='CAUTION, source file is overwritten!')
    parser.add_argument('source', help='DSM, DTM or opgaande elementen raster to perform filtering on',
                        type=str)
    parser.add_argument('target', type=str, choices=[k for k, _ in VECTOR_SOURCES.items()], nargs='+',
                        help='vector source to guide filtering')
    parser.add_argument('minimum_value', help='minimum value in source raster for valid filtering.', type=int)
    args = parser.parse_args()

    raise NotImplementedError('Niet geimplementeerd, want wat moet er teruggelegd worden op gefilterde plekken in geval van DSM/DTM?')

    # for target in args.target:
    #     filter_opgaande_elementen_based_on_vectors(
    #         vector_source=target,
    #         target_raster_src=args.source,
    #         minimum_value=args.minimum_value
    #     )
