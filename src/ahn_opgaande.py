import os
import numpy as np
import rasterio as rio

try:
    from majority_filter import filter_opgaande_elementen_based_on_vectors
    from read_write_windows import get_rw_windows
    from ahn_read import get_opgaande_elementen, verify_matching_rasters
    from hoogtes_dbf import build_heights_table, heights_table_to_file
    from flt_header import replace_hdr
    from output_profile import build_output_profile, get_ahn_files
except ModuleNotFoundError:
    from sample.geslotenheid.ahn_opgaande_elemenden.ahn_read import (
        get_opgaande_elementen,
        verify_matching_rasters,
    )
    from sample.geslotenheid.ahn_opgaande_elemenden.read_write_windows import (
        get_rw_windows,
    )
    from sample.geslotenheid.ahn_opgaande_elemenden.hoogtes_dbf import (
        build_heights_table,
        heights_table_to_file,
    )


def build_opgaande_elementenkaart(**kwargs):
    """
    Wrapper function to build opgaande elementenkaart from AHN DSM-DTM

    Parameters
    ----------
    kwargs

    Returns
    -------

    """

    output_profile, data_source = build_output_profile(**kwargs)
    output_profile.update(dtype={"flt": np.float32, "tif": np.uint16}[kwargs["of"]])
    u_values = set()

    with rio.open(
        os.path.join(kwargs["out_dir"], f'{kwargs["out_name"]}.{kwargs["of"]}'),
        "w",
        **output_profile,
    ) as dest:
        dest.update_tags(**kwargs)

        if data_source == "file":
            windows = get_rw_windows(
                target_profile=output_profile,
                sample=kwargs["sample"],
                window_width=kwargs["w_width"],
                window_height=kwargs["w_height"],
            )

            dsm = rio.open(kwargs["dsm_src"])
            dtm = rio.open(kwargs["dtm_src"])
            verify_matching_rasters(raster_a=dtm, raster_b=dsm)

            for i, w in enumerate(windows, start=1):
                print(f"processing window {i}/{len(windows)}", end="\r")
                opg = get_opgaande_elementen(
                    dsm=dsm,
                    dtm=dtm,
                    minimum_difference=kwargs["min_diff"],
                    uniform_height=kwargs["u_height"],
                    maaiveld_value=kwargs["mv_value"],
                    window=w.rio_window,
                )
                dest.write(opg, 1, window=w.rio_window)

                u_values.update(np.unique(opg[opg != kwargs["mv_value"]]))

        if data_source == "directory":
            i = 0
            for dsm_src, dtm_src in get_ahn_files(**kwargs):
                dsm = rio.open(dsm_src)
                dtm = rio.open(dtm_src)
                verify_matching_rasters(raster_a=dtm, raster_b=dsm)

                print(f"processing DSM-DTM file {i}/?", end="\r")
                opg = get_opgaande_elementen(
                    dsm=dsm,
                    dtm=dtm,
                    minimum_difference=kwargs["min_diff"],
                    uniform_height=kwargs["u_height"],
                    maaiveld_value=kwargs["mv_value"],
                )
                dest.write(
                    opg,
                    1,
                    window=rio.windows.from_bounds(
                        left=dsm.bounds.left,
                        bottom=dsm.bounds.bottom,
                        right=dsm.bounds.right,
                        top=dsm.bounds.top,
                        transform=output_profile["transform"],
                    ),
                )

                u_values.update(np.unique(opg[opg != kwargs["mv_value"]]))

                i += 1

    heights_table_to_file(
            os.path.join(kwargs["out_dir"], f'{kwargs["out_name"]}_heights.dbf'),
            build_heights_table(
                height_values=list(u_values), maaiveld_value=kwargs["mv_value"]
            ),
        )

    if kwargs["of"] == "flt":
        replace_hdr(
            dir=kwargs["out_dir"],
            hdrname=kwargs["out_name"],
            output_profile=output_profile,
        )


if __name__ == "__main__":
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("dsm_src", type=str, help="path to AHN DSM.")
    parser.add_argument("dtm_src", type=str, help="path to AHN DTM")
    parser.add_argument("out_name", type=str, help="output filename.")
    parser.add_argument("out_dir", type=str, help="output directory.")
    parser.add_argument(
        "--sample", type=int, help="n sample blocks. Default 0.", default=0
    )
    parser.add_argument(
        "--min_diff", type=int, help="minimum DSM-DTM difference. Default 2.", default=2
    )
    parser.add_argument(
        "--mv_value", type=int, help="Value for maaiveld. Default 0.", default=0
    )
    parser.add_argument(
        "--w_width", type=int, help="Window width in pxls. Default 1000.", default=1000
    )
    parser.add_argument(
        "--w_height", type=int, help="Window height in pxls. Default 500.", default=500
    )
    parser.add_argument(
        "--of",
        choices=["flt", "tif"],
        help="Output format. Default GeoTiff",
        default="tif",
    )
    parser.add_argument(
        "--u_height",
        type=int,
        default=0,
        help="Uniform height for all opgaande elementen.",
    )
    parser.add_argument(
        "--majority_filter",
        help="Majority filter below a powerlines and/or windturbines.",
        type=str, choices=['windturbines', 'powerlines'], nargs='+'

    )
    args = parser.parse_args()

    try:
        build_opgaande_elementenkaart(**vars(args))
        if args.majority_filter:
            for target in args.majority_filter:
                filter_opgaande_elementen_based_on_vectors(vector_source=target,
                                                           target_raster_src=os.path.join(args.out_dir, f'{args.out_name}.{args.of}'),
                                                           **vars(args))
        print(r"Done \O/")
    except (ValueError, AssertionError) as e:
        print(e)
        sys.exit(0)
