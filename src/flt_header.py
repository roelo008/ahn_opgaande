import os


def replace_hdr(dir: str, hdrname: str, output_profile: dict):
    """
    replace a header file with header format compatible with ViewScape
    :param dir: destination directory
    :param f: header filename
    :param output_profile:
    :return:
    """

    x_lower_left, y_lower_left = output_profile["transform"] * (
        0,
        output_profile["height"],
    )

    hdr = (
        f"ncols         {output_profile['width']}\n"
        f"nrows         {output_profile['height']}\n"
        f"xllcorner     {x_lower_left}\n"
        f"yllcorner     {y_lower_left}\n"
        f"cellsize      {output_profile['transform'].a}\n"
        f"NODATA_value  {output_profile['nodata']}\n"
        "byteorder     LSBFIRST"
    )

    with open(os.path.join(dir, f"{hdrname}_viewscape.hdr"), "w") as f:
        f.write(hdr)
